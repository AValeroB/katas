import java.util.HashMap;
import java.util.Map;

public class Game {

    public String winner(String[] deckSteve, String[] deckJosh) {

        Map<Character, Integer> rankOrder = new HashMap<>();
        
        rankOrder.put('2', 0);
        rankOrder.put('3', 1);
        rankOrder.put('4', 2);
        rankOrder.put('5', 3);
        rankOrder.put('6', 4);
        rankOrder.put('7', 5);
        rankOrder.put('8', 6);
        rankOrder.put('9', 7);
        rankOrder.put('T', 8);
        rankOrder.put('J', 9);
        rankOrder.put('Q', 10);
        rankOrder.put('K', 11);
        rankOrder.put('A', 12);

        int steveScore = 0;
        int joshScore = 0;

        for (int i = 0; i < deckJosh.length; i++) {

            char cardSteve = deckSteve[i].charAt(0);
            char cardJosh = deckJosh[i].charAt(0);

            if (rankOrder.get(cardSteve) > rankOrder.get(cardJosh)) {
                steveScore += 1;
            }

            else if (rankOrder.get(cardJosh) > rankOrder.get(cardSteve)) {
                joshScore += 1;
            }
        }

        if (steveScore > joshScore) {
            String steveWon = String.format("Steve wins %d to %d", steveScore, joshScore);
            return steveWon;
        }

        else if (joshScore > steveScore) {
            String joshWon = String.format("Josh wins %d to %d", joshScore, steveScore);
            return joshWon;
        }

        else {
            return "Tie";
        }
      }
}
